package com.ggjy.controller;

import com.common.util.privated.util.CommonUtil;
import com.common.util.privated.util.FastJsonConvertUtil;
import com.common.util.privated.util.LogUtil;
import com.common.util.privated.util.privated.dto.DataInfo;
import com.common.util.privated.util.privated.dto.DataInfoByLayuiTable;
import com.common.util.privated.util.privated.dto.ErrorInfo;
import com.common.util.privated.vo.LayuiTableVo;
import com.ggjy.enums.ErrorMsg;
import com.ggjy.exception.BaseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 输出控制器
 * @author len
 *
 */
@Component
@Scope("prototype")
public class OutputController {
	@Autowired
	private HttpServletRequest request;
	@Autowired
	private HttpServletResponse response;
	
	private HttpServletResponse getResponse() {
        response.setContentType("application/json;charset=UTF-8");
        return response;
	}
	
	public HttpServletRequest getRequest() {
		return request;
	}

	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}

	public void setResponse(HttpServletResponse response) {
		this.response = response;
	}

	/**
	 * 向前端输出json数据
	 * @param data 需要输出的数据
	 * @return
	 * @throws Exception
	 */
	public void outputData(Object data) throws Exception {
		DataInfo<Object> dataInfo = new DataInfo<>();
		dataInfo.setCode(ErrorMsg.SUCCEED.getId());
		dataInfo.setMessage(ErrorMsg.SUCCEED.getDesc());
		dataInfo.setDatas(data);
		this.getResponse().getWriter().write(FastJsonConvertUtil.convertObjectToJSONSString(dataInfo));
	}
	
	/**
	 * 向前端输出json数据，针对layui表格控件
	 * @param data 需要输出的数据
	 * @return
	 * @throws Exception
	 */
	public <T> void outputDataByLayuiTable(LayuiTableVo<T> data) throws Exception {
		DataInfoByLayuiTable<Object> dataInfoByLayuiTable = new DataInfoByLayuiTable<Object>();
		dataInfoByLayuiTable.setCode(ErrorMsg.SUCCEED.getId());
		dataInfoByLayuiTable.setMessage(ErrorMsg.SUCCEED.getDesc());
		dataInfoByLayuiTable.setDatas(data.getList());
		dataInfoByLayuiTable.setCount(data.getCount());
		this.getResponse().getWriter().write(FastJsonConvertUtil.convertObjectToJSONSString(dataInfoByLayuiTable));
	}
	
	/**
	 * 向前端输出错误信息，不记录错误日志
	 * @param errorCode 错误编码
	 * @param errorMsg 错误信息
	 */
	public void outputError(String errorCode, String errorMsg) throws Exception{
		ErrorInfo<String> errorInfo = new ErrorInfo<String>();
		errorInfo.setMessage(errorMsg);
		errorInfo.setCode(errorCode);
		this.getResponse().getWriter().write(FastJsonConvertUtil.convertObjectToJSONSString(errorInfo));
	}
	
	/**
	 * 输出错误信息
	 * 当e为null时处理普通错误信息，反之处理系统错误信息
	 * @param e
	 * @param errorMsg
	 */
	public void outputError(Exception e,String errorMsg) throws Exception{
		ErrorInfo<String> errorInfo = new ErrorInfo<String>();
		try {
			if(CommonUtil.isEmpty(e)) {
				errorInfo = this.disposeOrdinaryError(errorMsg);
			}else {
				errorInfo = this.dispostSystemError(e, errorMsg);
			}
			
			this.getResponse().getWriter().write(FastJsonConvertUtil.convertObjectToJSONSString(errorInfo));
		}catch(Exception e1) {
			LogUtil.logException(e1, e1.getMessage());
			errorInfo = new ErrorInfo<>();
			errorInfo.setCode(ErrorInfo.ERROR);
			errorInfo.setMessage(e1.getMessage());
			this.getResponse().getWriter().write(FastJsonConvertUtil.convertObjectToJSONSString(errorInfo));
		}
	}
	
	/**
	 * 处理系统错误信息
	 * @param e
	 * @param errorMsg
	 * @return
	 * @throws Exception
	 */
	private ErrorInfo<String> dispostSystemError(Exception e,String errorMsg) throws Exception {
		ErrorInfo<String> errorInfo = new ErrorInfo<>();
		String url = "";
		if(!CommonUtil.isEmpty(request) && !CommonUtil.isEmpty(request.getRequestURL())) {
			url = request.getRequestURL().toString();
		}
		if(e instanceof BaseException) {
			LogUtil.logException(e, "自定义错误,url:"+url+"======="+((BaseException)e).getJsonData());
	    	errorInfo.setCode(((BaseException)e).getErrorCode());
		}else {
			LogUtil.logException(e, "系统错误,操作失败,url:"+url);
	    	errorInfo.setCode(ErrorInfo.ERROR);
		}
		if(!CommonUtil.isEmpty(errorMsg)) {
    		errorInfo.setMessage(errorMsg);
    	}else {
    		errorInfo.setMessage(e.getMessage());
    	}
		return errorInfo;
	}
	
	/**
	 * 处理一般错误
	 * @param errorMsg
	 * @return
	 * @throws Exception
	 */
	private ErrorInfo<String> disposeOrdinaryError(String errorMsg) throws Exception {
		LogUtil.writeLog("请求返回错误信息："+errorMsg);
		ErrorInfo<String> errorInfo = new ErrorInfo<String>();
		errorInfo.setMessage(errorMsg);
		if(errorMsg.equals(ErrorMsg.NOT_LOGIN_ERROR.getDesc())) {
			errorInfo.setCode(ErrorMsg.NOT_LOGIN_ERROR.getId());
		}else {
			errorInfo.setCode(ErrorMsg.ORDINARY_ERROR.getId());
		}
		
		return errorInfo;
	}
}
