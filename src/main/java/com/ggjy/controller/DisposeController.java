package com.ggjy.controller;

import com.common.util.privated.util.CommonUtil;
import com.ggjy.exception.ParamException;
import com.ggjy.service.OperationService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

/**
 * 客户控制类
 *
 * @author len
 */
@SuppressWarnings("DuplicatedCode")
@Controller
@RequestMapping("/dispose")
public class DisposeController  {
    /**
     * 输出控制
     */
    private OutputController output;

    @Resource(name = "operationServiceImpl")
    private OperationService operationService;

    public DisposeController(OutputController output) {
        this.output = output;
    }



    /**
     * 操作档案柜
     * @param id 档案柜id（档案柜号）
     * @param opType 操作类型 1-开架；2-合架
     * @throws Exception 抛出异常
     */
    @RequestMapping(value = "/dispose", produces = {"application/json;charset=UTF-8"}, method = RequestMethod.POST)
    @ResponseBody
    public void dispose(Integer id, Integer opType) throws Exception {
        if(CommonUtil.isEmpty(id) || CommonUtil.isEmpty(opType)) {
            throw new ParamException("id:"+id+"=====opType:"+opType,null);
        }
        operationService.dispose(id,opType);
        output.outputData("操作成功");
    }

//    /**
//     * 操作档案柜
//     * @param id 档案柜id（档案柜号）
//     * @param opType 操作类型 1-开架；2-合架
//     * @throws Exception 抛出异常
//     */
//    @RequestMapping(value = "/queryStatus", produces = {"application/json;charset=UTF-8"}, method = RequestMethod.POST)
//    @ResponseBody
//    public void queryStatus(Integer id, Integer opType) throws Exception {
//
//    }






}
