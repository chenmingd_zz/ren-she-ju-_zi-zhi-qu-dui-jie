package com.ggjy.controller;

import com.ggjy.service.CSVService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

/**
 * CSV控制类
 *
 * @author len
 */
@SuppressWarnings("DuplicatedCode")
@Controller
@RequestMapping("/csv")
public class CSVController {
    /**
     * 输出控制
     */
    private OutputController output;

    @Resource(name = "csvServiceImpl")
    private CSVService csvService;

    public CSVController(OutputController output) {
        this.output = output;
    }



    /**
     * 输出CSV文件
     * @throws Exception 抛出异常
     */
    @RequestMapping(value = "/exportCSVCompany", produces = {"application/json;charset=UTF-8"}, method = RequestMethod.GET)
    @ResponseBody
    public void exportCSVCompany() throws Exception {
        //输出公司CSV文件
        csvService.excuteZizhiqu();
        output.outputData("操作成功");
    }

    /**
     * 输出天地图CSV文件
     * @throws Exception 抛出异常
     */
    @RequestMapping(value = "/excuteTianditu", produces = {"application/json;charset=UTF-8"},
            method = RequestMethod.GET)
    @ResponseBody
    public void excuteTianditu() throws Exception {
        //输出公司CSV文件
        csvService.excuteTianditu();
        output.outputData("操作成功");
    }







}
