package com.ggjy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZizhiquDataApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(ZizhiquDataApplication.class, args);
	}
}
