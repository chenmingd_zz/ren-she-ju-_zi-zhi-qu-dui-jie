package com.ggjy.entity;

/**
 * TODO
 *
 * @author root
 * @date 2021/1/18
 */
public class PersonInfo {
    private String companyIntroduction;
    private long id;
    private String agencyName;
    private long industryId;
    private long enterpriseNatureId;
    private String indacommRegAdd;
    private String loborPerName;
    private String laborContacPhone;
    private String email;


    public String getCompanyIntroduction() {
        return companyIntroduction;
    }

    public void setCompanyIntroduction(String companyIntroduction) {
        this.companyIntroduction = companyIntroduction;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getAgencyName() {
        return agencyName;
    }

    public void setAgencyName(String agencyName) {
        this.agencyName = agencyName;
    }

    public long getIndustryId() {
        return industryId;
    }

    public void setIndustryId(long industryId) {
        this.industryId = industryId;
    }

    public long getEnterpriseNatureId() {
        return enterpriseNatureId;
    }

    public void setEnterpriseNatureId(long enterpriseNatureId) {
        this.enterpriseNatureId = enterpriseNatureId;
    }

    public String getIndacommRegAdd() {
        return indacommRegAdd;
    }

    public void setIndacommRegAdd(String indacommRegAdd) {
        this.indacommRegAdd = indacommRegAdd;
    }

    public String getLoborPerName() {
        return loborPerName;
    }

    public void setLoborPerName(String loborPerName) {
        this.loborPerName = loborPerName;
    }

    public String getLaborContacPhone() {
        return laborContacPhone;
    }

    public void setLaborContacPhone(String laborContacPhone) {
        this.laborContacPhone = laborContacPhone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
