package com.ggjy.entity;

import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;

@Table
public class UnitBasicInfo {
    private BigDecimal id;

    private BigDecimal version;

    private String addr;

    private String agencyName;

    private Double annualIncome;

    private Date auditDate;

    private Long auditStatus;

    private BigDecimal belongsDepartmentId;

    private String busiRegistLicense;

    private String certCode;

    private Date createDate;

    private Short credibilityRecord;

    private String email;

    private Short employmentPolicy;

    private BigDecimal employmentPolicyContentId;

    private BigDecimal enterpriseNatureId;

    private String fax;

    private Date foundTime;

    private String indacommRegAdd;

    private BigDecimal indTategoryId;

    private BigDecimal industryId;

    private Short isProbatUnit;

    private String laborContacPhone;

    private String legalRepresentative;

    private String licenseCategory;

    private String licensePictureUuid;

    private String loborPerName;

    private String majorScope;

    private String mobile;

    private BigDecimal personnelScaleId;

    private BigDecimal probatUnitContentId;

    private String qqNumber;

    private Short recommendSign;

    private Double regFunAmout;

    private String url;

    private BigDecimal userInfoId;

    private String liveAddress;

    private String liveStatus;

    private String roomId;

    private String companyIntroduction;

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public BigDecimal getVersion() {
        return version;
    }

    public void setVersion(BigDecimal version) {
        this.version = version;
    }

    public String getAddr() {
        return addr;
    }

    public void setAddr(String addr) {
        this.addr = addr == null ? null : addr.trim();
    }

    public String getAgencyName() {
        return agencyName;
    }

    public void setAgencyName(String agencyName) {
        this.agencyName = agencyName == null ? null : agencyName.trim();
    }

    public Double getAnnualIncome() {
        return annualIncome;
    }

    public void setAnnualIncome(Double annualIncome) {
        this.annualIncome = annualIncome;
    }

    public Date getAuditDate() {
        return auditDate;
    }

    public void setAuditDate(Date auditDate) {
        this.auditDate = auditDate;
    }

    public Long getAuditStatus() {
        return auditStatus;
    }

    public void setAuditStatus(Long auditStatus) {
        this.auditStatus = auditStatus;
    }

    public BigDecimal getBelongsDepartmentId() {
        return belongsDepartmentId;
    }

    public void setBelongsDepartmentId(BigDecimal belongsDepartmentId) {
        this.belongsDepartmentId = belongsDepartmentId;
    }

    public String getBusiRegistLicense() {
        return busiRegistLicense;
    }

    public void setBusiRegistLicense(String busiRegistLicense) {
        this.busiRegistLicense = busiRegistLicense == null ? null : busiRegistLicense.trim();
    }

    public String getCertCode() {
        return certCode;
    }

    public void setCertCode(String certCode) {
        this.certCode = certCode == null ? null : certCode.trim();
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Short getCredibilityRecord() {
        return credibilityRecord;
    }

    public void setCredibilityRecord(Short credibilityRecord) {
        this.credibilityRecord = credibilityRecord;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email == null ? null : email.trim();
    }

    public Short getEmploymentPolicy() {
        return employmentPolicy;
    }

    public void setEmploymentPolicy(Short employmentPolicy) {
        this.employmentPolicy = employmentPolicy;
    }

    public BigDecimal getEmploymentPolicyContentId() {
        return employmentPolicyContentId;
    }

    public void setEmploymentPolicyContentId(BigDecimal employmentPolicyContentId) {
        this.employmentPolicyContentId = employmentPolicyContentId;
    }

    public BigDecimal getEnterpriseNatureId() {
        return enterpriseNatureId;
    }

    public void setEnterpriseNatureId(BigDecimal enterpriseNatureId) {
        this.enterpriseNatureId = enterpriseNatureId;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax == null ? null : fax.trim();
    }

    public Date getFoundTime() {
        return foundTime;
    }

    public void setFoundTime(Date foundTime) {
        this.foundTime = foundTime;
    }

    public String getIndacommRegAdd() {
        return indacommRegAdd;
    }

    public void setIndacommRegAdd(String indacommRegAdd) {
        this.indacommRegAdd = indacommRegAdd == null ? null : indacommRegAdd.trim();
    }

    public BigDecimal getIndTategoryId() {
        return indTategoryId;
    }

    public void setIndTategoryId(BigDecimal indTategoryId) {
        this.indTategoryId = indTategoryId;
    }

    public BigDecimal getIndustryId() {
        return industryId;
    }

    public void setIndustryId(BigDecimal industryId) {
        this.industryId = industryId;
    }

    public Short getIsProbatUnit() {
        return isProbatUnit;
    }

    public void setIsProbatUnit(Short isProbatUnit) {
        this.isProbatUnit = isProbatUnit;
    }

    public String getLaborContacPhone() {
        return laborContacPhone;
    }

    public void setLaborContacPhone(String laborContacPhone) {
        this.laborContacPhone = laborContacPhone == null ? null : laborContacPhone.trim();
    }

    public String getLegalRepresentative() {
        return legalRepresentative;
    }

    public void setLegalRepresentative(String legalRepresentative) {
        this.legalRepresentative = legalRepresentative == null ? null : legalRepresentative.trim();
    }

    public String getLicenseCategory() {
        return licenseCategory;
    }

    public void setLicenseCategory(String licenseCategory) {
        this.licenseCategory = licenseCategory == null ? null : licenseCategory.trim();
    }

    public String getLicensePictureUuid() {
        return licensePictureUuid;
    }

    public void setLicensePictureUuid(String licensePictureUuid) {
        this.licensePictureUuid = licensePictureUuid == null ? null : licensePictureUuid.trim();
    }

    public String getLoborPerName() {
        return loborPerName;
    }

    public void setLoborPerName(String loborPerName) {
        this.loborPerName = loborPerName == null ? null : loborPerName.trim();
    }

    public String getMajorScope() {
        return majorScope;
    }

    public void setMajorScope(String majorScope) {
        this.majorScope = majorScope == null ? null : majorScope.trim();
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile == null ? null : mobile.trim();
    }

    public BigDecimal getPersonnelScaleId() {
        return personnelScaleId;
    }

    public void setPersonnelScaleId(BigDecimal personnelScaleId) {
        this.personnelScaleId = personnelScaleId;
    }

    public BigDecimal getProbatUnitContentId() {
        return probatUnitContentId;
    }

    public void setProbatUnitContentId(BigDecimal probatUnitContentId) {
        this.probatUnitContentId = probatUnitContentId;
    }

    public String getQqNumber() {
        return qqNumber;
    }

    public void setQqNumber(String qqNumber) {
        this.qqNumber = qqNumber == null ? null : qqNumber.trim();
    }

    public Short getRecommendSign() {
        return recommendSign;
    }

    public void setRecommendSign(Short recommendSign) {
        this.recommendSign = recommendSign;
    }

    public Double getRegFunAmout() {
        return regFunAmout;
    }

    public void setRegFunAmout(Double regFunAmout) {
        this.regFunAmout = regFunAmout;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url == null ? null : url.trim();
    }

    public BigDecimal getUserInfoId() {
        return userInfoId;
    }

    public void setUserInfoId(BigDecimal userInfoId) {
        this.userInfoId = userInfoId;
    }

    public String getLiveAddress() {
        return liveAddress;
    }

    public void setLiveAddress(String liveAddress) {
        this.liveAddress = liveAddress == null ? null : liveAddress.trim();
    }

    public String getLiveStatus() {
        return liveStatus;
    }

    public void setLiveStatus(String liveStatus) {
        this.liveStatus = liveStatus == null ? null : liveStatus.trim();
    }

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId == null ? null : roomId.trim();
    }

    public String getCompanyIntroduction() {
        return companyIntroduction;
    }

    public void setCompanyIntroduction(String companyIntroduction) {
        this.companyIntroduction = companyIntroduction == null ? null : companyIntroduction.trim();
    }
}