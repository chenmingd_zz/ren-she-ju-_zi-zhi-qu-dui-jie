package com.ggjy.entity;

import javax.persistence.Table;
import java.math.BigDecimal;

/**
 * TODO
 *
 * @author root
 * @date 2021/1/19
 */
@Table
public class PositionInfo {
    //单位id
    private BigDecimal id;
    //公司名称
    private String agencyName;
    //联系电话
    private String phone;
    //职位名称
    private String occupationalName;
    //职位类别
    private BigDecimal occupationalType;
    //薪酬福利
    private String remuneration;
    //职位介绍
    private String jobDescription;
    //工作地点
    private BigDecimal workSite;
    //工作经验
    private String workExperience;
    //学历要求id
    private BigDecimal educationId;
    //招聘人数
    private BigDecimal recNum;
    //最低月薪
    private Long minimumMoney;
    //发布日期
    private String startDate;
    //更新日期
    private String updateDate;
    //有效期
    private String activeTime;
    //有效状态
    private Integer effectiveStatus;
    //专业
    private BigDecimal major;
    //岗位状态
    private Integer positionStatus;
    //职位类别名称
    private String occupationalTypeName;
    //工作经验名称
    private String workExperienceName;
    //学历要求名称
    private String educationName;
    //职位表id
    private BigDecimal positionId;

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public String getOccupationalName() {
        return occupationalName;
    }

    public void setOccupationalName(String occupationalName) {
        this.occupationalName = occupationalName;
    }

    public BigDecimal getOccupationalType() {
        return occupationalType;
    }

    public void setOccupationalType(BigDecimal occupationalType) {
        this.occupationalType = occupationalType;
    }

    public String getRemuneration() {
        return remuneration;
    }

    public void setRemuneration(String remuneration) {
        this.remuneration = remuneration;
    }

    public String getJobDescription() {
        return jobDescription;
    }

    public void setJobDescription(String jobDescription) {
        this.jobDescription = jobDescription;
    }

    public BigDecimal getWorkSite() {
        return workSite;
    }

    public void setWorkSite(BigDecimal workSite) {
        this.workSite = workSite;
    }

    public String getWorkExperience() {
        return workExperience;
    }

    public void setWorkExperience(String workExperience) {
        this.workExperience = workExperience;
    }

    public BigDecimal getEducationId() {
        return educationId;
    }

    public void setEducationId(BigDecimal educationId) {
        this.educationId = educationId;
    }

    public BigDecimal getRecNum() {
        return recNum;
    }

    public void setRecNum(BigDecimal recNum) {
        this.recNum = recNum;
    }

    public Long getMinimumMoney() {
        return minimumMoney;
    }

    public void setMinimumMoney(Long minimumMoney) {
        this.minimumMoney = minimumMoney;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public String getActiveTime() {
        return activeTime;
    }

    public void setActiveTime(String activeTime) {
        this.activeTime = activeTime;
    }

    public Integer getEffectiveStatus() {
        return effectiveStatus;
    }

    public void setEffectiveStatus(Integer effectiveStatus) {
        this.effectiveStatus = effectiveStatus;
    }

    public BigDecimal getMajor() {
        return major;
    }

    public void setMajor(BigDecimal major) {
        this.major = major;
    }

    public Integer getPositionStatus() {
        return positionStatus;
    }

    public void setPositionStatus(Integer positionStatus) {
        this.positionStatus = positionStatus;
    }

    public String getOccupationalTypeName() {
        return occupationalTypeName;
    }

    public void setOccupationalTypeName(String occupationalTypeName) {
        this.occupationalTypeName = occupationalTypeName;
    }

    public String getWorkExperienceName() {
        return workExperienceName;
    }

    public void setWorkExperienceName(String workExperienceName) {
        this.workExperienceName = workExperienceName;
    }

    public String getEducationName() {
        return educationName;
    }

    public void setEducationName(String educationName) {
        this.educationName = educationName;
    }

    public BigDecimal getPositionId() {
        return positionId;
    }

    public void setPositionId(BigDecimal positionId) {
        this.positionId = positionId;
    }

    public String getAgencyName() {
        return agencyName;
    }

    public void setAgencyName(String agencyName) {
        this.agencyName = agencyName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
