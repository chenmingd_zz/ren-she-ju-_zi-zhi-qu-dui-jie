package com.ggjy.task;

import com.ggjy.service.CSVService;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 按分钟执行的调度任务
 * 
 * @author len
 *
 */
@Component
public class TaskMinute {

	@Resource(name = "csvServiceImpl")
	private CSVService csvService;

	/**
	 * 增加线程池线程数量，默认为1
	 * @return csvService.excuteZizhiqu();
	 */
	@Bean
	public TaskScheduler taskScheduler() {
	    ThreadPoolTaskScheduler taskScheduler = new ThreadPoolTaskScheduler();
	    taskScheduler.setPoolSize(50);
	    return taskScheduler;
	}


	
	/**
	 * 发送当日统计信息
	 * 每天02点发送一次
	 */
	@Scheduled(cron="0 01 2 * * *")
	public void sendStatistical() throws Exception {
		//自治区数据推送
		csvService.excuteZizhiqu();
		//天地图数据推送
		csvService.excuteTianditu();
		System.out.println("cron定时任务"+Thread.currentThread().getName());
	}
	

	

}
