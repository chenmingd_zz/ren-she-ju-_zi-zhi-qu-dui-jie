package com.ggjy.service;

import com.alibaba.fastjson.JSONObject;

/**
 * TODO
 *
 * @author root
 * @date 2020/9/29
 */
public interface OperationService {
    /**
     * 开架或合架
     * @param id 档案柜号
     * @param opType 操作类型 1-开架；2-合架
     * @throws Exception 抛出异常
     */
    void dispose(Integer id, Integer opType) throws Exception;

    /**
     * 查询档案柜实时状态
     * @param id 档案柜id
     * @throws Exception 抛出异常
     */
    JSONObject getStatus(Integer id) throws Exception;
}
