package com.ggjy.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.ggjy.enums.ErrorMsg;
import com.ggjy.exception.GenerallyException;
import com.ggjy.service.OperationService;
import com.ggjy.util.IntelligentCabinet;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * TODO
 *
 * @author root
 */
@SuppressWarnings("RedundantThrows")
@Service(value = "operationServiceImpl")
public class OperationServiceImpl implements OperationService {
    /**
     * 1区列
     */
    public static final int[] LIST1 = {22,21,20,19,18,17,16,15,14,13,12,11,10,9,8,7,6,5,4,3,2,1};

    /**
     * 开架或合架
     * @param id 档案柜号
     * @param opType 操作类型 1-开架；2-合架
     * @throws Exception 抛出异常
     */
    @Override
    public void dispose(Integer id, Integer opType) throws Exception {
        //获取参数
        Map<String,Object> paramsMap = formatParam(id);

        //唤醒操作
        IntelligentCabinet.awake(paramsMap.get("url").toString(),
                paramsMap.get("storeCode").toString(),
                Integer.parseInt(paramsMap.get("quNo").toString()));

        //查看密集柜状态
        Map<String,Object> resMap = isCheck(id);
        if(resMap.get("res").toString().equals("0")) {
            throw new GenerallyException(ErrorMsg.PARAM_ERROR.getId(),resMap.get("message").toString(),null,null);
        }

        if(opType==2) {
            //合架
            IntelligentCabinet.reset(paramsMap.get("url").toString(),paramsMap.get("storeCode").toString(),
                    Integer.parseInt(paramsMap.get("quNo").toString()));
        }else if(opType==1) {
            //开架
            IntelligentCabinet.open(paramsMap.get("url").toString(),paramsMap.get("storeCode").toString(),
                    Integer.parseInt(paramsMap.get("quNo").toString()),
                    Integer.parseInt(paramsMap.get("direction").toString()),
                    Integer.parseInt(paramsMap.get("colNo").toString()));
        }
    }

    /**
     * 查询档案柜实时状态
     * @param id 档案柜id
     * @throws Exception 抛出异常
     */
    @Override
    public JSONObject getStatus(Integer id) throws Exception {
        //获取参数
        Map<String,Object> paramsMap = formatParam(id);
        return IntelligentCabinet.getStatus(paramsMap.get("url").toString(),
                paramsMap.get("storeCode").toString(),
                Integer.parseInt(paramsMap.get("quNo").toString()));
    }

    /**
     * 判断档案柜是否报警或被锁
     * @param id 档案柜id
     * @return 验证结果 0-异常 1-正常
     */
    private Map<String,Object> isCheck(Integer id) throws Exception {
        JSONObject jsonObject = this.getStatus(id);
        String message = "正常";
        int res = 1;
        //报警判断
        if(jsonObject.getJSONObject("data").getString("ISBJ").equals("1")) {
            message = "密集柜有报警，无法操作";
            res = 0;
        }
        //锁定判断
        if(jsonObject.getJSONObject("data").getString("ISLOCK").equals("1")) {
            message = "密集柜被锁定，无法操作";
            res = 0;
        }
//        //通电判断
//        if(jsonObject.getJSONObject("data").getString("IsPower").equals("0")) {
//            message = "设备未通电，无法操作";
//            res = 0;
//        }

        Map<String,Object> resMap = new HashMap<>();
        resMap.put("message",message);
        resMap.put("res",res);
        return resMap;
    }

    /**
     * 格式化参数
     * @param id 档案柜id
     * @return 参数json
     */
    private Map<String,Object> formatParam(Integer id) throws Exception {
        //档案柜
        float knob = Float.parseFloat(id+"");
        //密集柜的列号
        int num;
        //请求地址
        String url;
        //库房编码
        String storeCode;
        //区号
        int quNo;
        //开架方向 1-左；2-右
        int direction;
        //开架接口
        if(knob<46) {
            //1区
            url = "http://192.168.117.243:15000";
            storeCode = "12345678";
            direction = 1;
            quNo = 1;
            num = (int) Math.ceil(knob/2);
            if(num>LIST1.length) {
                throw new GenerallyException(ErrorMsg.PARAM_ERROR.getId(),id+"列在最外侧，请手动打开",null,null);
            }
            num = LIST1[num-1];
        }else if(knob>=46 && knob<=88) {
            //2区
            url = "http://192.168.117.242:16000";
            storeCode = "12345671";
            direction = 1;
            quNo = 2;
            num = getLie(id);
        }else {
            //3区
            url = "http://192.168.117.247:15000";
            storeCode = "12345672";
            direction = 2;
            quNo = 3;
            num = getLie(id);
        }

        Map<String, Object> params = new HashMap<>();
        params.put("storeCode",storeCode);
        params.put("quNo",quNo);
        params.put("colNo",num);
        params.put("direction",direction);
        params.put("url",url);

        return params;
    }

    /**
     * 获取需要操作的列的编号
     * @param id 档案柜号
     * @return 列号
     * @throws Exception 抛出异常
     */
    private int getLie(int id) throws Exception {
        /*
         *由于电子档案柜的列和档案柜编号的对应关系随时可能变，所以这里就以map写死，不做算法
         */
        //返回列数
        int num = 0;
        //B区的列和档案柜对应关系
        if(id==46) {
            throw new GenerallyException(ErrorMsg.PARAM_ERROR.getId(),id+"列在最外侧，请手动打开",null,null);
        }
        Map<String,Object> BMap = getB();
        if(id>=46 && id<=88) {
            //B区
            num = Integer.parseInt(BMap.get(id+"").toString());
        }

        //C区的列和档案柜对应关系
        Map<String,Object> CMap = getC();
        if(id>=89 && id<=132) {
            //C区
            num = Integer.parseInt(CMap.get(id+"").toString());
        }
        return num;


    }
    /**
     * C区电子档案柜列号与档案柜号对应关系
     * 由于电子档案柜的列和档案柜编号的对应关系随时可能变，所以这里就以map写死，不做算法
     */
    private Map<String, Object> getC() {
        Map<String,Object> CMap = new HashMap<>();
        CMap.put("89",23);
        CMap.put("90",23);
        CMap.put("91",22);
        CMap.put("92",22);
        CMap.put("93",21);
        CMap.put("94",21);
        CMap.put("95",20);
        CMap.put("96",20);
        CMap.put("97",19);
        CMap.put("98",19);
        CMap.put("99",18);
        CMap.put("100",18);
        CMap.put("101",17);
        CMap.put("102",17);
        CMap.put("103",16);
        CMap.put("104",16);
        CMap.put("105",15);
        CMap.put("106",15);
        CMap.put("107",14);
        CMap.put("108",14);
        CMap.put("109",13);
        CMap.put("110",13);
        CMap.put("111",12);
        CMap.put("112",12);
        CMap.put("113",11);
        CMap.put("114",11);
        CMap.put("115",10);
        CMap.put("116",10);
        CMap.put("117",9);
        CMap.put("118",9);
        CMap.put("119",8);
        CMap.put("120",8);
        CMap.put("121",7);
        CMap.put("122",7);
        CMap.put("123",6);
        CMap.put("124",6);
        CMap.put("125",5);
        CMap.put("126",5);
        CMap.put("127",4);
        CMap.put("128",4);
        CMap.put("129",3);
        CMap.put("130",3);
        CMap.put("131",2);
        CMap.put("132",2);
        return CMap;
    }

    /**
     * B区电子档案柜列号与档案柜号对应关系
     * 由于电子档案柜的列和档案柜编号的对应关系随时可能变，所以这里就以map写死，不做算法
     */
    private Map<String,Object> getB() {
        Map<String,Object> BMap = new HashMap<>();
        BMap.put("47",21);
        BMap.put("48",21);
        BMap.put("49",20);
        BMap.put("50",20);
        BMap.put("51",19);
        BMap.put("52",19);
        BMap.put("53",18);
        BMap.put("54",18);
        BMap.put("55",17);
        BMap.put("56",17);
        BMap.put("57",16);
        BMap.put("58",16);
        BMap.put("59",15);
        BMap.put("60",15);
        BMap.put("61",14);
        BMap.put("62",14);
        BMap.put("63",13);
        BMap.put("64",13);
        BMap.put("65",12);
        BMap.put("66",12);
        BMap.put("67",11);
        BMap.put("68",11);
        BMap.put("69",10);
        BMap.put("70",10);
        BMap.put("71",9);
        BMap.put("72",9);
        BMap.put("73",8);
        BMap.put("74",8);
        BMap.put("75",7);
        BMap.put("76",7);
        BMap.put("77",6);
        BMap.put("78",6);
        BMap.put("79",5);
        BMap.put("80",5);
        BMap.put("81",4);
        BMap.put("82",4);
        BMap.put("83",3);
        BMap.put("84",3);
        BMap.put("85",2);
        BMap.put("86",2);
        BMap.put("87",1);
        BMap.put("88",1);
        return BMap;
    }
}
