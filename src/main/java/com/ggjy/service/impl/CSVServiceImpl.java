package com.ggjy.service.impl;

import com.common.util.privated.util.LogUtil;
import com.ggjy.dao.master.UnitBasicInfoMapper;
import com.ggjy.entity.PositionInfo;
import com.ggjy.entity.UnitBasicInfo;
import com.ggjy.service.CSVService;
import com.ggjy.util.CSVOperateUtil;
import com.ggjy.util.CommonUtil;
import com.ggjy.util.SFTPFileTransferUtil;
import org.springframework.stereotype.Service;

import java.io.File;
import java.math.BigDecimal;
import java.util.*;
import java.util.logging.Logger;

/**
 * TODO
 *
 * @author root
 * @date 2021/1/18
 */
@Service(value = "csvServiceImpl")
public class CSVServiceImpl implements CSVService {

    /*
     * 公司报送文件本地路径
     */
    private static final String COMPANY_PATH = "C:/zizhiqu/ab01.csv";
    private static final String POSITION_PATH = "C:/zizhiqu/cb21.csv";
    private static final String RESUME_PATH = "C:/zizhiqu/cc20.csv";
    private static final String FAIR_PATH1 = "C:/zizhiqu/cb21wlzp.csv";
    private static final String FAIR_PATH2 = "C:/zizhiqu/cc20wlzp.csv";

    /**
     * 天地图报送文件路径
     */
    private static final String TIANDITU = "C:/tianditu/cb21.csv";

    private UnitBasicInfoMapper unitBasicInfoMapper;

    public CSVServiceImpl(UnitBasicInfoMapper _unitBasicInfoMapper) {
        this.unitBasicInfoMapper = _unitBasicInfoMapper;
    }

    @Override
    public void excuteZizhiqu() throws Exception {
        //远程主机ip
        String remoteHost = "xjrc365.com";
        //登录用户名
        String username = "klmy_site";
        //登陆密码
        String password = "M6wx5^d5VGxT";
        //ssh协议默认端口
        int remotePort = 12346;
        //上传到远程的文件路径，要保证登录用户有写权限
        String remoteFile = "/";

        //公司导出csv文件
        exportCSVCompany();
        //将csv文件上传至服务器
        uploadToService(remoteHost,username,password,remotePort,COMPANY_PATH,remoteFile);

        //岗位导出csv文件
        exportCSVPosition();
        //将csv文件上传至服务器
        uploadToService(remoteHost,username,password,remotePort,POSITION_PATH,remoteFile);

        try {
            //招聘会职位导出csv文件
            exportCSVFairPosition();
            //将csv文件上传至服务器
            uploadToService(remoteHost,username,password,remotePort,FAIR_PATH1,remoteFile);

            //招聘会简历导出csv文件
            exportCSVFairResume();
            //将csv文件上传至服务器
            uploadToService(remoteHost,username,password,remotePort,FAIR_PATH2,remoteFile);
        }catch (Exception e) {
            LogUtil.logException(e,"招聘会导出csv出错");
        }


        //简历导出csv文件
        exportCSVResume();
        //将csv文件上传至服务器
        uploadToService(remoteHost,username,password,remotePort,RESUME_PATH,remoteFile);
    }

    /**
     * 天地图执行
     * @throws Exception 抛出异常
     */
    @Override
    public void excuteTianditu() throws Exception {
        //远程主机ip
        String remoteHost = "117.146.112.204";
        //登录用户名
        String username = "tdtuser";
        //登陆密码
        String password = "tdtuser@123";
        //ssh协议默认端口
        int remotePort = 10022;
        //上传到远程的文件路径，要保证登录用户有写权限
        String remoteFile = "/";

        /*招聘信息*/
        exportCSVPositionCommon(CSVServiceImpl.TIANDITU);
        //将csv文件上传至服务器
        uploadToService(remoteHost,username,password,remotePort,TIANDITU,remoteFile);

    }

    /**
     * 输出招聘会职位csv
     */
    private void exportCSVFairPosition() throws Exception {
        //输出文件
        File file = new File(CSVServiceImpl.FAIR_PATH1);

        //数据集合
        List<Map<String,Object>> dataList = new ArrayList<>();
        //查询条件
        Map<String,Object> params = new HashMap<>();
        //查询数据
        List<Map<String,Object>> list = unitBasicInfoMapper.getFairForPositionList(params);
        for (Map<String,Object> positionMap : list) {
            //去除换行符，将英文逗号换成中文逗号
            formatField(positionMap);

            dataList.add(positionMap);
        }
        //标题
        String[] header = {"wlzp001","acb210"};
        //标题对应的数据库字段
        String[] field = {"TYPE","ID"};
        //参数封装
        Map<String,Object> dataMap = new HashMap<>();
        dataMap.put("header",header);
        dataMap.put("dataList",dataList);
        dataMap.put("field",field);
        //导出至文件
        CSVOperateUtil.exportToFile(file,dataMap);
    }

    /**
     * 输出招聘会简历csv
     */
    private void exportCSVFairResume() throws Exception {
        //输出文件
        File file = new File(CSVServiceImpl.FAIR_PATH2);

        //数据集合
        List<Map<String,Object>> dataList = new ArrayList<>();
        //查询条件
        Map<String,Object> params = new HashMap<>();
        //查询数据
        List<Map<String,Object>> list = unitBasicInfoMapper.getFairForResumeList(params);
        for (Map<String,Object> resumeMap : list) {
            //去除换行符，将英文逗号换成中文逗号
            formatField(resumeMap);

            dataList.add(resumeMap);
        }
        //标题
        String[] header = {"wlzp001","acc200"};
        //标题对应的数据库字段
        String[] field = {"TYPE","ID"};
        //参数封装
        Map<String,Object> dataMap = new HashMap<>();
        dataMap.put("header",header);
        dataMap.put("dataList",dataList);
        dataMap.put("field",field);
        //导出至文件
        CSVOperateUtil.exportToFile(file,dataMap);
    }

    /**
     * 输出简历csv
     */
    private void exportCSVResume() throws Exception {
        //输出文件
        File file = new File(CSVServiceImpl.RESUME_PATH);

        //数据集合
        List<Map<String,Object>> dataList = new ArrayList<>();
        List<Map<String,Object>> list = unitBasicInfoMapper.getResumeList();
        for (Map<String,Object> resumeMap : list) {
            //去除换行符，将英文逗号换成中文逗号
            formatField(resumeMap);

            dataList.add(resumeMap);
        }
        //标题
        String[] header = {"acc200","aac002","aac003","aac004","aac005","aac006","aca111","acb215",
                "aab022","aab019","acc217","aac011","acc264","aae043","aae044"};
        //标题对应的数据库字段
        String[] field = {"id","certCode","name","sex","nation","birthday","intentionPosition",
                "intentionSite","intentionIndustrial","intentionNature","workExperience",
                "educationId","major","regDate","updataDate"};
        //参数封装
        Map<String,Object> dataMap = new HashMap<>();
        dataMap.put("header",header);
        dataMap.put("dataList",dataList);
        dataMap.put("field",field);
        //导出至文件
        CSVOperateUtil.exportToFile(file,dataMap);
    }

    /**
     * 输出职位csv文件
     */
    private void exportCSVPosition() throws Exception {
        //输出文件
        File file = new File(CSVServiceImpl.POSITION_PATH);

        //数据集合
        List<LinkedHashMap<String,Object>> dataList = new ArrayList<>();
        //查询参数
        Map<String,Object> paramMap = new HashMap<>();
        String currDate = CommonUtil.dataFormat("yyyyMMdd");
        paramMap.put("currDate",currDate);
        List<PositionInfo> list = unitBasicInfoMapper.getListPosition(paramMap);
        for (PositionInfo positionInfo : list) {
            //格式化招聘人数
            BigDecimal recNum = positionInfo.getRecNum();
            if(recNum==null || recNum.intValue() <= 0) {
                positionInfo.setRecNum(BigDecimal.valueOf(1));
            }
            //薪酬福利
            String remuneration = positionInfo.getRemuneration();
            //最低月薪替换汉字
            remuneration = remuneration.replaceAll("以上","");
            remuneration = remuneration.replaceAll("以下","");
            //最低月薪
            Long minimumMoney = remuneration.split("-")[0]==null ? 0L :
                    Long.parseLong(remuneration.split("-")[0]);
            //赋值最低月薪
            positionInfo.setMinimumMoney(minimumMoney);
            //对象转map
            LinkedHashMap<String, Object> map = CommonUtil.bean2map(positionInfo);
            //去除换行符，将英文逗号换成中文逗号
            formatField(map);

            dataList.add(map);
        }
        //标题
        String[] header = {"aab001","cb213","aca111","acb214","acb216",
                "acb215","acc217","aac011","acb21g","acb21l","aae043","aae044",
                "validity","aae100","acc264","ycb211","aca112","acc217_dsc","aac011_dsc","acb210"};
        //标题对应的数据库字段
        String[] field = {"id","occupationalName","occupationalType","remuneration","jobDescription","workSite",
                "workExperience","educationId","recNum","minimumMoney","startDate","updateDate","activeTime",
                "effectiveStatus","major","positionStatus","occupationalTypeName","workExperienceName","educationName",
                "positionId"};
        //参数封装
        Map<String,Object> dataMap = new HashMap<>();
        dataMap.put("header",header);
        dataMap.put("dataList",dataList);
        dataMap.put("field",field);
        //导出至文件
        CSVOperateUtil.exportToFile(file,dataMap);
    }

    /**
     * 输出职位csv文件,通用方法
     */
    @SuppressWarnings("SameParameterValue")
    private void exportCSVPositionCommon(String path) throws Exception {
        //输出文件
        File file = new File(path);

        //数据集合
        List<LinkedHashMap<String,Object>> dataList = new ArrayList<>();
        //查询参数
        Map<String,Object> paramMap = new HashMap<>();
        String currDate = CommonUtil.dataFormat("yyyyMMdd");
        paramMap.put("currDate",currDate);
        List<PositionInfo> list = unitBasicInfoMapper.getListPositionTianditu(paramMap);
        for (PositionInfo positionInfo : list) {
            //格式化招聘人数
            BigDecimal recNum = positionInfo.getRecNum();
            if(recNum==null || recNum.intValue() <= 0) {
                positionInfo.setRecNum(BigDecimal.valueOf(1));
            }
            //薪酬福利
            String remuneration = positionInfo.getRemuneration();
            //最低月薪替换汉字
            remuneration = remuneration.replaceAll("以上","");
            remuneration = remuneration.replaceAll("以下","");
            //最低月薪
            Long minimumMoney = remuneration.split("-")[0]==null ? 0L :
                    Long.parseLong(remuneration.split("-")[0]);
            //赋值最低月薪
            positionInfo.setMinimumMoney(minimumMoney);
            //对象转map
            LinkedHashMap<String, Object> map = CommonUtil.bean2map(positionInfo);
            //去除换行符，将英文逗号换成中文逗号
            formatField(map);

            dataList.add(map);
        }
        //标题
        String[] header = {"cb211","cb212","cb213","acb214","acb216",
                "acb21g","acb21l","aae043","aae044",
                "validity","aae100","ycb211","aca112","acc217_dsc","aac011_dsc"};
        //标题对应的数据库字段
        String[] field = {"agencyName","phone","occupationalName","remuneration","jobDescription",
                "recNum","minimumMoney","startDate","updateDate",
                "activeTime","effectiveStatus","positionStatus","occupationalTypeName","workExperienceName","educationName"};
        //参数封装
        Map<String,Object> dataMap = new HashMap<>();
        dataMap.put("header",header);
        dataMap.put("dataList",dataList);
        dataMap.put("field",field);
        //导出至文件
        CSVOperateUtil.exportToFile(file,dataMap);
    }

    /**
     * 输出公司csv文件
     * @throws Exception 抛出异常
     */
    private void exportCSVCompany() throws Exception {
        //输出文件
        File file = new File(CSVServiceImpl.COMPANY_PATH);

        //数据集合
        List<LinkedHashMap<String,Object>> dataList = new ArrayList<>();
        List<UnitBasicInfo> list = unitBasicInfoMapper.getListCompany();
        for (UnitBasicInfo unitBasicInfo : list) {
            //对象转map
            LinkedHashMap<String, Object> map = CommonUtil.bean2map(unitBasicInfo);
            formatField(map);

            dataList.add(map);
        }
        //标题
        String[] header = {"aab001","aab004","aab092","aab022","aab019","aae006","aae004","aae005","aae159"};
        //标题对应的数据库字段
        String[] field = {"id","agencyName","companyIntroduction","industryId","enterpriseNatureId","indacommRegAdd",
        "loborPerName","laborContacPhone","email"};
        //参数封装
        Map<String,Object> dataMap = new HashMap<>();
        dataMap.put("header",header);
        dataMap.put("dataList",dataList);
        dataMap.put("field",field);
        //导出至文件
        CSVOperateUtil.exportToFile(file,dataMap);
    }

    /**
     * 上传文件至服务器
     * @param remoteHost 远程主机ip
     * @param username 登录用户名
     * @param password 登陆密码
     * @param remotePort ssh协议默认端口
     * @param localFile 本地文件路径
     * @param remoteFile 上传到远程的文件路径，要保证登录用户有写权限
     */
    private void uploadToService(String remoteHost, String username, String password,
                            int remotePort, String localFile, String remoteFile) throws Exception {
        SFTPFileTransferUtil sftp = new SFTPFileTransferUtil();
        sftp.setPassword(password);
        sftp.setLocalFile(localFile);
        sftp.setRemoteFile(remoteFile);
        sftp.setRemotePort(remotePort);
        sftp.setUsername(username);
        sftp.setRemoteHost(remoteHost);
        sftp.upload();
    }

    /**
     * 去除换行符等
     * @param map 需要操作的map
     */
    private void formatField(Map<String,Object> map) {
        for(Map.Entry<String,Object> entry : map.entrySet()) {
            if(entry.getValue()!=null) {
                map.put(entry.getKey(),entry.getValue().toString().replaceAll("\r",""));
                map.put(entry.getKey(),entry.getValue().toString().replaceAll("\n",""));
                map.put(entry.getKey(),entry.getValue().toString().replaceAll("\t",""));
                map.put(entry.getKey(),entry.getValue().toString().replaceAll(",","，"));
            }
        }
    }
}
