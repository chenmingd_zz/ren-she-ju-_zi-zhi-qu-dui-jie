package com.ggjy.service;

/**
 * TODO
 *
 * @author root
 * @date 2021/1/18
 */
public interface CSVService {

    /**
     * 执行自治区操作
     * @throws Exception 抛出异常
     */
    void excuteZizhiqu() throws Exception;

    /**
     * 执行天地图文件共享操作
     * @throws Exception 抛出异常
     */
    void excuteTianditu() throws Exception;
}
