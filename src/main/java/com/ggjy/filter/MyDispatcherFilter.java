package com.ggjy.filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 
 * 功能:修复"检测到目标URL存在http host头攻击漏洞"
 * 
 * @author len
 *
 */
@WebFilter(filterName = "myDispatcherFilter", urlPatterns = "/*")
public class MyDispatcherFilter extends HttpServlet implements Filter {

	private static final long serialVersionUID = 2599411860361877379L;

	public void doFilter(ServletRequest req, ServletResponse res,
			FilterChain chain) throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) res;
		// 不过滤的url
		//String host = request.getHeader("host");
		if (!canAccess(request)) {
			//System.out.println("1:" + host);
			response.setStatus(302);
		} else {
			//System.out.println("2:" + host);
			String url = request.getRequestURL().toString();
			if (url.endsWith("/mobile/") || url.endsWith("/mobile"))
				response.sendRedirect((url + "/login.html").replace(
						"//login.html", "/login.html"));
			else if (url.endsWith("/sxlw/") || url.endsWith("/sxlw"))
				response.sendRedirect((url + "/index.jsp").replace(
						"//index.jsp", "/index.jsp"));
			else
				chain.doFilter(req, res);
		}
	}

	/**
	 * 
	 * 功能:允许通过的IP
	 * 
	 * @author zjq
	 * @date 2015-6-3
	 * @param request
	 * @return
	 */
	private boolean canAccess(HttpServletRequest request) {
		String host = request.getHeader("host");
		//System.out.println(host);
		// String url = String.valueOf(request.getRequestURL());
		boolean flag = host.contains("127.0.0.1")
				;
		return flag;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.Filter#init(javax.servlet.FilterConfig)
	 */
	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub

	}

}
