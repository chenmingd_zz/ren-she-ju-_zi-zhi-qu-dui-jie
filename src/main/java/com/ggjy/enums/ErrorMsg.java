package com.ggjy.enums;


@SuppressWarnings("SpellCheckingInspection")
public enum ErrorMsg {
	SUCCEED("200","操作成功"), 
    NOT_FOUND("404","Not Found!!!"),
    SYSTEM_ERROR("500","系统错误"),
    FORBIDDEN_ERROR("403","403!!!"),
    ORDINARY_ERROR("1000","一般错误"),
    PARAM_ERROR("1001","参数错误"),
	SELECT_ERROR("1002","查询失败"),
	INSERT_ERROR("1003","插入失败"),
	USER_EXIST_ERROR("1004","用户已存在,请更换用户名"),
	DELETE_ERROR("1005","删除失败"),
	ACCOUNT_PASSWORD_ERROR("1006","用户名或密码错误"),
	ROLE_MENU_ERROR("1007","权限不足，请切换用户或重新登录"),
	SESSION_OUT_ERROR("1008","session过期，请刷新页面重新登录"),
	UPDATE_ERROR("1009","修改失败"),
	MOBILE_EXIST_ERROR("1011","手机号已存在"),
	TOKEN_ERROR("1012","令牌已过期，请重新登录"),
	USER_INFO_EMPTY("1013","用户信息为空"),
	OLD_PASSWORD_EMPTY("1014","旧密码不能为空"),
	NEW_PASSWORD_EMPTY("1015","新密码不能为空"),
	OLD_PASSWORD_ERROR("1016","旧密码输入错误"),
	MODIFY_PASSWORD_FAIL("1017","密码修改失败"),
	PARENT_EQUALS_CHILD("1018","父分类id不能与子分类id相同，如果您想新建分类，请点击添加分类按钮"),
	IMPORT_EXCLE_NOT_INEXISTENCE("1019","导入excle出错[文件不存在]"),
	IMPORT_EXCLE_NAME_ERROR("1020","模板错误，请使用下载的模板进行导入"),
	ADD_EQUIPMENT_ERROR("1021","添加设备失败，设备信息不完整"),
	IMAGE_CODE_ERROR("1022","图片验证码输入错误"),
	PARAM_TOTAL_ERROR("1023","参数数量不匹配"),
	JSON_PASE_ERROR("1024","json解析出错"),
	SECURITY_ERROR("1025","权限验证出错"),
	NOT_LOGIN_ERROR("1026","您目前没有登录，请登录"),
	NEW_PASSWORD_ERROR("1027","两次输入的新密码不一致"),
	GOODS_SPEC_DO_NOT_DEL("1028","颜色规格不允许删除"),
	ADMIN_DO_NOT_DEL("1029","管理员用户不允许删除"),
	SUPER_ROLE_DO_NOT_DEL("1030","超级管理员角色不允许删除"),
	UPLOAD_ERROR("1031","上传失败"),
	REQUEST_ERROR("1032","请求失败"),
	SMS_SEND_ERROR("1033","短信发送失败,请稍后重试"),
	SMS_CODE_ERROR("1034","短信验证码输入错误或过期"),
	DEEP_ERROR("1035","菜单最多三级，请不要添加第四级菜单"),
	SUPER_USER_ERROR("1036","超级管理员已存在，请不要重复创建"),
	VOLUNTEER_TEAM_JOIN("1037","您已加入了服务队，请不要重复申请"),
	VOLUNTEER_TEAM_NOWCHECK("1038","您的申请正在审核，请不要重复申请"),
	CURRENCY_INSUFFICIENT("1039","很抱歉，您的爱心币不足"),
	COMPLAINTS_PENDING_AGREED("1040","工单处于投诉中，不允许进行操作"),
	END_DATE_ERROR("1041","开始时间与结束时间必须是同一天"),
	GREATER_NOWDATE_ERROR("1042","开始日期不能小于当前日期"),
	START_END_DATE_ERROR("1043","开始时间不能大于结束时间"),
	ORDER_ALREADY_RECEIVE("1044","该工单已经被其他人领取"),
	VOLUNTEER_SERVICE_TIME_ERROR("1045","志愿者服务申请时间为早上10点以后，晚上21点之前"),
	FILE_SIZE_ERROR("1046","上传文件大小超过限制，请上传低于%sMB的文件"),
	TYPE_ERROR("1047","分类必须填写"),
	CONTENT_AUDIO_ERROR("1048","内容或音频必须填写其中一个"),
	COMPLETE_USER_INFO_ERROR("1049","请先完善用户信息"),
	IDENTITY_USER_INFO_ERROR("1050","请先完善用户身份信息"),
	OCR_ERROR("1051","图片解析失败"),
	FLOW_CREATE_EXCEPTION("1052","部分流程创建失败"),
	FLOW_TASK_INEXISTENCE("1053","流程不存在"),
	FLOW_FAIL("1055","流程执行失败"),
	FLOW_TASKID_INEXISTENCE("1056","任务不存在")
	;

    private String id;

    private String desc;

	ErrorMsg(String id, String desc) {
        this.id=id;
        this.desc=desc;
    }

    public static ErrorMsg getInstance(String id) {
        for(ErrorMsg tmp: ErrorMsg.values()) {
            if(tmp.id.equals(id) ) {
                return tmp;
            }
        }
        return null;
    }

    public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc=desc;
    }
}
