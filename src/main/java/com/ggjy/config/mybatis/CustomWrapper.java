package com.ggjy.config.mybatis;

import com.google.common.base.CaseFormat;
import org.apache.ibatis.reflection.MetaObject;
import org.apache.ibatis.reflection.wrapper.MapWrapper;

import java.util.Map;

/**
 * 继承类 MapWrapper,重写findProperty,通过useCamelCaseMapping来判断是否开启使用驼峰
 * 需要配置mybatis.configuration.map-underscore-to-camel-case=true
 * @author len
 *
 */
public class CustomWrapper extends MapWrapper {

	public CustomWrapper(MetaObject metaObject, Map<String, Object> map) {
		super(metaObject, map);
	}
	
	@Override
	public String findProperty(String name, boolean useCamelCaseMapping) {
		if(useCamelCaseMapping){
			String strFormat = name;
			//判断name中是否带"_"，不带下划线则不进行处理
			if(name.contains("_")) {
				//CaseFormat是引用的 guava库,里面有转换驼峰的,免得重复造轮子
				strFormat = CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.LOWER_CAMEL,name);
			}
			
			return strFormat;
		}
		return name;
	}
}
