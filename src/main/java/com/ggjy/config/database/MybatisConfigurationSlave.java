package com.ggjy.config.database;
//package com.login.config.database;
//
//import javax.annotation.Resource;
//import javax.sql.DataSource;
//
//import org.apache.ibatis.session.SqlSessionFactory;
//import org.mybatis.spring.boot.autoconfigure.MybatisAutoConfiguration;
//import org.springframework.boot.autoconfigure.AutoConfigureAfter;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
//import tk.mybatis.spring.annotation.MapperScan;
//
///**
// * 数据库服务类
// * @author len
// *
// */
//@Configuration
//@AutoConfigureAfter(value=DataSourceConfiguration.class)
//@MapperScan(basePackages = {"com.bhz.mail.mapper.mail_slave"}, sqlSessionFactoryRef = "slaveSqlSessionFactory")
//public class MybatisConfigurationMailSlave extends MybatisAutoConfiguration {
//	
//	@Resource(name="masterDataSource")
//	private DataSource masterDataSource;
//	
//	@Resource(name="slaveDataSource")
//	private DataSource slaveDataSource;
//	
//	@Bean(name="slaveSqlSessionFactory")
//	public SqlSessionFactory slaveSqlSessionFactory() throws Exception {
//		//return null;
//		return super.sqlSessionFactory(slaveDataSource);
//	}
//	
//}
