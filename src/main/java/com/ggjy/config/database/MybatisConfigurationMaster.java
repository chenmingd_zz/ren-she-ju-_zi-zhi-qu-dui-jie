package com.ggjy.config.database;

import com.ggjy.config.mybatis.MapWrapperFactory;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.boot.autoconfigure.MybatisAutoConfiguration;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import tk.mybatis.spring.annotation.MapperScan;

import javax.annotation.Resource;
import javax.sql.DataSource;

/**
 * 数据库服务类
 * @author len
 *
 */
@Configuration
@AutoConfigureAfter(value=DataSourceConfiguration.class)
@MapperScan(basePackages = {"com.ggjy.dao.master"}, sqlSessionFactoryRef = "masterSqlSessionFactory")
public class MybatisConfigurationMaster extends MybatisAutoConfiguration {
	
	@Resource(name="masterDataSource")
	private DataSource masterDataSource;
	
//	@Resource(name="slaveDataSource")
//	private DataSource slaveDataSource;
	
	@Bean(name="masterSqlSessionFactory")
	@Primary
	public SqlSessionFactory masterSqlSessionFactory() throws Exception {
		SqlSessionFactory sqlSessionFactory = super.sqlSessionFactory(masterDataSource);
		//处理map转驼峰标识
		sqlSessionFactory.getConfiguration().setObjectWrapperFactory(new MapWrapperFactory());
		return sqlSessionFactory;
	}
//	@Bean(name="slaveSqlSessionFactory")
//	public SqlSessionFactory slaveSqlSessionFactory() throws Exception {
//		//return null;
//		return super.sqlSessionFactory(slaveDataSource);
//	}
	
}
