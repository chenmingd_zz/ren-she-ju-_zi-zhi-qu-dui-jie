package com.ggjy.exception;

import com.ggjy.enums.ErrorMsg;

/**
 * 参数错误
 * @author len
 *
 */
public class ParamException extends BaseException {


	public ParamException(String jsonData, Throwable cause) {
		super(ErrorMsg.PARAM_ERROR.getId(), "操作失败，请查看档案柜状态", jsonData, cause);
	}

	private static final long serialVersionUID = 1L;

	

}
