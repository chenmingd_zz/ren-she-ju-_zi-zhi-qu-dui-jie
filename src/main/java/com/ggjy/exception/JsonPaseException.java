package com.ggjy.exception;

import com.ggjy.enums.ErrorMsg;

/**
 * json解析异常
 * @author len
 *
 */
public class JsonPaseException extends BaseException {
	
	private static final long serialVersionUID = 1L;
	
	public JsonPaseException( String jsonData, Throwable cause) {
		super(ErrorMsg.JSON_PASE_ERROR.getId(), ErrorMsg.JSON_PASE_ERROR.getDesc(), jsonData, cause);
	}

}
