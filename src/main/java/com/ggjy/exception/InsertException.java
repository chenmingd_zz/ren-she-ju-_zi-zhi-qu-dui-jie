package com.ggjy.exception;

import com.ggjy.enums.ErrorMsg;

/**
 * 插入数据库异常
 * @author len
 *
 */
public class InsertException extends BaseException {


	public InsertException(String jsonData, Throwable cause) {
		super(ErrorMsg.INSERT_ERROR.getId(), ErrorMsg.INSERT_ERROR.getDesc(), jsonData, cause);
	}

	private static final long serialVersionUID = 1L;

	

}
