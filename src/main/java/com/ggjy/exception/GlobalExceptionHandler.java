package com.ggjy.exception;

import com.ggjy.controller.OutputController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import java.net.ConnectException;

@ControllerAdvice
public class GlobalExceptionHandler {
	/**
	 * 输出控制类
	 */
	@Autowired
	private OutputController output;
	
	/**
	 * 统一处理一般性异常，不需要记录日志
	 * @param e
	 * @param message
	 * @param req
	 */
	private void disposeGenerallyException(BaseException e, String message, HttpServletRequest req) throws Exception {
		output.outputError(e.getErrorCode(), e.getMessage());
	}
	
	/**
     * 统一处理自定义异常
     * @param e 异常e
     * @param msg 异常信息
     * @param req request
     * @return 
     * @throws Exception
     */
    private void disposeException(BaseException e, String msg, HttpServletRequest req) throws Exception {
    	output.outputError(e, msg);
    }
    
    /**
     * 处理500异常
     * @param req
     * @param e
     * @throws Exception
     */
    @ExceptionHandler(value = Exception.class)
    public void defaultErrorHandler(HttpServletRequest req, Exception e) throws Exception {
		output.outputError(e, "系统错误，请与管理员联系");
    	
    }

    /**
     * 多个参数不匹配异常
     * @param req
     * @param e
     * @return
     * @throws Exception
     */
    @ExceptionHandler(value = ArgumentsTotalException.class)
    public void ArgumentsTotalExceptionHandler(HttpServletRequest req, ArgumentsTotalException e) throws Exception {
    	this.disposeException(e, e.getMessage(), req);
    }
    
    /**
     * 数据库插入失败异常
     * @param req
     * @param e
     * @return
     * @throws Exception
     */
    @ExceptionHandler(value = InsertException.class)
    public void InsertExceptionHandler(HttpServletRequest req, InsertException e) throws Exception {
    	this.disposeException(e, e.getMessage(), req);
    }
    
    /**
     * json解析异常
     * @param req
     * @param e
     * @return
     * @throws Exception
     */
    @ExceptionHandler(value = JsonPaseException.class)
    public void jsonPaseExceptionHandler(HttpServletRequest req, JsonPaseException e) throws Exception {
    	this.disposeException(e, e.getMessage(), req);
    }
    
    /**
     * 参数错误异常
     * @param req
     * @param e
     * @return
     * @throws Exception
     */
    @ExceptionHandler(value = ParamException.class)
    public void ParamExceptionHandler(HttpServletRequest req, ParamException e) throws Exception {
    	this.disposeException(e, e.getMessage(), req);
    }
    
    /**
     * 更新错误异常
     * @param req
     * @param e
     * @throws Exception
     */
    @ExceptionHandler(value=UpdateException.class)
    public void UpdateExceptionHandler(HttpServletRequest req, UpdateException e) throws Exception {
    	this.disposeException(e, e.getMessage(), req);
    }
    
    /**
     * 上传错误异常
     * @param req
     * @param e
     * @throws Exception
     */
    @ExceptionHandler(value=UploadException.class)
    public void UploadExceptionHandler(HttpServletRequest req, UploadException e) throws Exception {
    	this.disposeException(e, e.getMessage(), req);
    }
    

    /**
     * 一般性异常,不需要记录日志
     * @param req
     * @param e
     * @throws Exception
     */
    @ExceptionHandler(value=GenerallyException.class)
    public void GenerallyExceptionHandler(HttpServletRequest req, GenerallyException e) throws Exception {
    	this.disposeGenerallyException(e, e.getMessage(), req);
    }

}