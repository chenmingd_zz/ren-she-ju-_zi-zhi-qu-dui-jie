package com.ggjy.exception;

import com.ggjy.enums.ErrorMsg;

/**
 * 鉴权失败
 * @author len
 *
 */
public class SecurityException extends BaseException {
	
	private static final long serialVersionUID = 1L;

	public SecurityException( String jsonData, Throwable cause) {
		super(ErrorMsg.SECURITY_ERROR.getId(), ErrorMsg.SECURITY_ERROR.getDesc(), jsonData, cause);
	}


}
