package com.ggjy.exception;

import com.ggjy.enums.ErrorMsg;

/**
 * 两个或多个参数,数量不匹配异常
 * @author len
 *
 */
public class ArgumentsTotalException extends BaseException {
	
	private static final long serialVersionUID = 1L;

	public ArgumentsTotalException(String jsonData, Throwable cause) {
		super(ErrorMsg.PARAM_TOTAL_ERROR.getId(), ErrorMsg.PARAM_TOTAL_ERROR.getDesc(), jsonData, cause);
	}

}
