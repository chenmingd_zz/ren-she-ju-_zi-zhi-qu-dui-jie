package com.ggjy.exception;

/**
 * 一般异常，不需要记录日志
 * @author len
 *
 */
public class GenerallyException extends BaseException {

	private static final long serialVersionUID = 1L;

	public GenerallyException(String errorCode, String message, String jsonData, Throwable cause) {
		super(errorCode, message, jsonData, cause);
	}

}
