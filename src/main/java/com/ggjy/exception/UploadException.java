package com.ggjy.exception;

import com.ggjy.enums.ErrorMsg;

/**
 * 上传图片异常
 * @author len
 *
 */
public class UploadException extends BaseException {


	public UploadException(String jsonData, Throwable cause) {
		super(ErrorMsg.UPLOAD_ERROR.getId(), ErrorMsg.UPLOAD_ERROR.getDesc(), jsonData, cause);
	}

	private static final long serialVersionUID = 1L;

	

}
