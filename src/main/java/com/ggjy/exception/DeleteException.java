package com.ggjy.exception;

import com.ggjy.enums.ErrorMsg;

/**
 * 删除数据异常
 * @author len
 *
 */
public class DeleteException extends BaseException {

	private static final long serialVersionUID = 1L;

	public DeleteException( String jsonData, Throwable cause) {
		super(ErrorMsg.DELETE_ERROR.getId(), ErrorMsg.DELETE_ERROR.getDesc(), jsonData, cause);
	}

}
