package com.ggjy.exception;

import com.ggjy.enums.ErrorMsg;

/**
 * 更新操作异常类
 * @author len
 *
 */
public class UpdateException extends BaseException {
	
	private static final long serialVersionUID = 1L;

	public UpdateException( String jsonData, Throwable cause) {
		super(ErrorMsg.UPDATE_ERROR.getId(), ErrorMsg.UPDATE_ERROR.getDesc(), jsonData, cause);
	}

}
