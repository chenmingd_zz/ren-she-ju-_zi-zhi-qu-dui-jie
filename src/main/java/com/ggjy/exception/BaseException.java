package com.ggjy.exception;

/**
 * 基础异常
 * 
 * @author len
 *
 */
public class BaseException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	/**
	 * 错误编码
	 */
	private String errorCode;

	/**
	 * 需要记录在日志中的数据，json格式
	 */
	private String jsonData;

	/**
	 * 构造一个基本异常.
	 *
	 * @param errorCode
	 *            错误编码
	 * @param message
	 *            信息描述
	 * @param jsonData
	 *            需要记录的数据，json格式
	 * @param cause
	 *            try catch 捕获的异常e
	 */
	public BaseException(String errorCode, String message, String jsonData, Throwable cause) {
		super(message, cause);
		this.setErrorCode(errorCode);
		this.setJsonData(jsonData);
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getJsonData() {
		return jsonData;
	}

	public void setJsonData(String jsonData) {
		this.jsonData = jsonData;
	}

}
