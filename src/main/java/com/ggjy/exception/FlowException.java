package com.ggjy.exception;

import com.ggjy.enums.ErrorMsg;

/**
 * 工作流异常
 * @author len
 *
 */
public class FlowException extends BaseException {


	public FlowException(ErrorMsg errorMsg, String jsonData, Throwable cause) {
		super(errorMsg.getId(), errorMsg.getDesc(), jsonData, cause);
	}

	private static final long serialVersionUID = 1L;

	

}
