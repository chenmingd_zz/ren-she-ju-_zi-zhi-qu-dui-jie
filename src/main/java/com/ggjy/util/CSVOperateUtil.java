package com.ggjy.util;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;

/**
 * csv操作工具类
 *
 * @author root
 * @date 2021/1/18
 */
public class CSVOperateUtil {

    /**
     * 导出至文件
     * 需要使用Map
     * @param file 要输出的文件
     * @param dataMap 导出的数据
     * @throws Exception 抛出异常
     */
    @SuppressWarnings("unchecked")
    public static void exportToFile(File file, Map<String,Object> dataMap) throws Exception {
        if(!file.exists()) {
            CommonUtil.createFile(file);
        }
        FileOutputStream fos = new FileOutputStream(file);
        OutputStreamWriter osw = new OutputStreamWriter(fos, StandardCharsets.UTF_8);
        //表头
        String[] header = (String[]) dataMap.get("header");
        //表头对应的字段
        String[] field = (String[]) dataMap.get("field");

        CSVFormat csvFormat = CSVFormat.DEFAULT.withHeader(header);
        CSVPrinter csvPrinter = new CSVPrinter(osw, csvFormat);

//        csvPrinter = CSVFormat.DEFAULT.withHeader("姓名", "年龄", "家乡").print(osw);
        //数据解析
        List<Map<String,Object>> dataList = (List<Map<String, Object>>) dataMap.get("dataList");
        for(Map<String,Object> map : dataList) {
            Object[] dataArr = new Object[header.length];
            int i = 0;
            for(String title : field) {
                dataArr[i] = map.get(title);
                i++;
            }
            csvPrinter.printRecord(dataArr);
        }

//        for (int i = 0; i < 10; i++) {
//            csvPrinter.printRecord("张三", 20, "湖北");
//        }

        csvPrinter.flush();
        csvPrinter.close();

    }
}
