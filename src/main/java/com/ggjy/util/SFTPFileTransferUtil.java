package com.ggjy.util;

import com.common.util.privated.util.LogUtil;
import com.jcraft.jsch.*;

/**
 * TODO
 *
 * @author root
 * @date 2021/1/18
 */
public class SFTPFileTransferUtil {
    //远程主机ip
    private String remoteHost = "xjrc365.com";
    //登录用户名
    private String username = "klmy_site";
    //登陆密码
    private String password = "M6wx5^d5VGxT";
    //ssh协议默认端口
    private int remotePort = 12346;
    //本地文件路径
    private String localFile = "C:/cjsworkspace/cjs-excel-demo/target/abc.csv";
    //上传到远程的文件路径，要保证登录用户有写权限
    String remoteFile = "/";
    //session超时时间
    private static final int SESSION_TIMEOUT = 10000;
    //管道流超时时间
    private static final int CHANNEL_TIMEOUT = 5000;

    public String getLocalFile() {
        return localFile;
    }

    public void setLocalFile(String localFile) {
        this.localFile = localFile;
    }

    public String getRemoteFile() {
        return remoteFile;
    }

    public void setRemoteFile(String remoteFile) {
        this.remoteFile = remoteFile;
    }

    public String getRemoteHost() {
        return remoteHost;
    }

    public void setRemoteHost(String remoteHost) {
        this.remoteHost = remoteHost;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getRemotePort() {
        return remotePort;
    }

    public void setRemotePort(int remotePort) {
        this.remotePort = remotePort;
    }

    public void upload() throws Exception {
        Session jschSession = null;
        try {

            JSch jsch = new JSch();
//            jsch.setKnownHosts("/home/zimug/.ssh/known_hosts");
            jschSession = jsch.getSession(username, remoteHost, remotePort);
            jschSession.setConfig("StrictHostKeyChecking", "no");

            // 通过ssh私钥的方式登录认证
            // jsch.addIdentity("/home/zimug/.ssh/id_rsa");

            // 通过密码的方式登录认证
            jschSession.setPassword(password);
            jschSession.connect(SESSION_TIMEOUT);

            Channel sftp = jschSession.openChannel("sftp");  //建立sftp文件传输管道
            sftp.connect(CHANNEL_TIMEOUT);

            ChannelSftp channelSftp = (ChannelSftp) sftp;

            // 传输本地文件到远程主机
            channelSftp.put(localFile, remoteFile);

            channelSftp.exit();
            CommonUtil.definedLog(true,"/usr/local/java_log","sft.log",
                    "上传成功，本地文件："+localFile+"=====远程路径："+remoteFile);
        } catch (SftpException | JSchException e) {
            CommonUtil.definedLog(true,"/usr/local/java_log","sft.log",
                    "上传失败，本地文件："+localFile+"=====远程路径："+remoteFile);
            LogUtil.logException(e, e.getMessage());
            e.printStackTrace();
        } finally {
            if (jschSession != null) {
                jschSession.disconnect();
            }
        }

    }
}
