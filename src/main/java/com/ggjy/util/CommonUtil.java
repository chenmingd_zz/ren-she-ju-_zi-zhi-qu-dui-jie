package com.ggjy.util;

import com.google.common.base.CaseFormat;
import org.springframework.util.ResourceUtils;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.io.*;
import java.lang.reflect.Field;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CommonUtil {
	
	/**
	 * 
	 * 功能:判断参数是否为空或null,
	 * 
	 * @author len
	 * @date 2016-1-21
	 * @param value
	 *            : String、List、Map判断是否为空和null，其他对象判断是否为null
	 * @return false不为空，true为空
	 */
	public static boolean isEmpty(Object value) throws Exception {
		if (value == null) {
			return true;
		} else {
			if (value instanceof String) {
				if (((String) value).trim().length() == 0) {
					return true;
				} else {
					return false;
				}
			} else if (value instanceof List) {
				if (((List<?>) value).isEmpty()) {
					return true;
				} else {
					return false;
				}
			} else if (value instanceof Map) {
				if (((Map<?, ?>) value).isEmpty()) {
					return true;
				} else {
					return false;
				}
			} else if (value instanceof String[]) {
				if (((String[]) value).length > 0) {
					return false;
				} else {
					return true;
				}
			} else if (value instanceof int[]) {
				if (((int[]) value).length > 0) {
					return false;
				} else {
					return true;
				}
			} else {
				return false;
			}
		}
	}
	
	/**
	 * 功能:使用utf-8进行url解码
	 * 
	 * @param encodeUrl
	 *            需要解码的url
	 * @return
	 * @throws Exception 
	 */
	public static String decodeUrl(String encodeUrl) throws Exception {
		String decodeStr = "";
		if (!CommonUtil.isEmpty(encodeUrl)) {
			try {
				decodeStr = URLDecoder.decode(encodeUrl, "utf-8");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
		}

		return decodeStr;
	}
	
	


	/**
	 * 功能: 将毫秒为单位的long日期转换成以秒为单位的long日期
	 *
	 * @param time
	 *            毫秒的lang日期
	 * @deprecated 此方法会造成一些意想不到的错误，新方法使用 {@link CommonUtil#millisecondToSecond(Long)}
	 */
	@Deprecated
	public static Long getLongTime(Long time) throws Exception {
		Long second = Long.parseLong((time + "").substring(0, (time + "").length() - 3));
		return second;
	}

	/**
	 * 功能: 将毫秒为单位的long日期转换成以秒为单位的long日期
	 *
	 * @param time
	 *            毫秒的lang日期
	 */
	public static Long millisecondToSecond(Long time) throws Exception {
		return time/1000L;
	}
	
	/**
	 * 获取根目录路径
	 * @return
	 * @throws Exception
	 */
	public static String getRootPath() throws Exception {
		//获取跟目录
		File path = new File(ResourceUtils.getURL("classpath:").getPath());
		if(!path.exists()) path = new File("");
		return path.getAbsolutePath();
	}
	
	/**
	 * 功能:自定义输出日志
	 * @param writeTime 是否显示日期
	 * @param path
	 *            日志路径
	 * @param fileName
	 *            日志名字
	 * @return 文件路径
	 * @throws IOException
	 */
	public static String definedLog(boolean writeTime, String path,
			String fileName, String content) throws Exception {
		/*
		 * 入参判断
		 */
		if (CommonUtil.isEmpty(path) || CommonUtil.isEmpty(fileName)
				|| CommonUtil.isEmpty(content)) {
			return null;
		}

		// 如果目录文件不存在则创建一个
		File file = new File(path, fileName);

		if (!file.exists()) {
			// 创建文件
			try {
				CommonUtil.createFile(file);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		if (file.exists()) {
			// 写入日志
			BufferedWriter bw = null;
			try {
				bw = new BufferedWriter(new OutputStreamWriter(
						new FileOutputStream(file, true), "UTF-8"));
				if(writeTime) {
					bw.write(CommonUtil.dataFormat("yyyy-MM-dd HH:mm:ss")+"记录日志##############");
					bw.newLine();
				}
				bw.write(content);
				bw.newLine();
				bw.flush();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					bw.close();
					// fw.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		return file.getPath();
	}

	/**
	 * 功能:创建文件
	 * 
	 * @param file
	 *            需要创建的文件
	 * @return
	 * @throws IOException
	 */
	public static void createFile(File file) throws Exception {
		// 判断文件目录是否存在，不存在则创建目录
		if (!file.getParentFile().exists()) {
			boolean creatDir = file.getParentFile().mkdirs();
			if (creatDir) {
				if (!file.exists()) {
					file.createNewFile();
				}
			}
		} else {
			if (!file.exists()) {
				file.createNewFile();
			}
		}
	}
	
	/**
	 * 功能:按照指定格式显示当前日期时间
	 * 
	 * @param formatStyle
	 *            时间显示的样式如:yyyy-MM-dd HH:mm:ss
	 * @return 格式化后的日期时间
	 */
	public static String dataFormat(String formatStyle) throws Exception {
		String dateStr = CommonUtil.dataFormat(formatStyle, System.currentTimeMillis());
		return dateStr;
	}
	
	/**
	 * 功能:按照指定格式显示指定日期时间
	 * 
	 * @param formatStyle
	 *            时间显示的样式如:yyyy-MM-dd HH:mm:ss
	 * @param dataTime 日期时间戳精确到毫秒
	 * @return 格式化后的日期时间
	 */
	public static String dataFormat(String formatStyle, Long dataTime) throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat(formatStyle);
		String dateStr = sdf.format(new Date(dataTime));
		return dateStr;
	}
	
	/**
	 * 功能:将指定的时间字符串转日期
	 * 
	 * @param formatStyle
	 *            时间显示的样式如:yyyy-MM-dd HH:mm:ss
	 * @param dataTime 时间字符串
	 * @return 格式化后的日期时间
	 */
	public static Date dataFormat(String formatStyle, String dataTime) throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat(formatStyle);
		Date date = sdf.parse(dataTime);
		return date;
	}
	
	/**
	 * 功能:将指定的日期格式化成指定的字符串
	 * 
	 * @param formatStyle
	 *            时间显示的样式如:yyyy-MM-dd HH:mm:ss
	 * @param date 日期对象
	 * @return 格式化后的日期时间
	 */
	public static String dataFormat(String formatStyle, Date date) throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat(formatStyle);
		String str = sdf.format(date);
		return str;
	}
	

	
	/**
	 * 获取ip
	 * 
	 * @param request
	 * @return
	 */
	public static String getIp(HttpServletRequest request) throws Exception {
		String ip = request.getHeader("X-Forwarded-For");
		if (!StringUtils.isEmpty(ip) && !"unKnown".equalsIgnoreCase(ip)) {
			// 多次反向代理后会有多个ip值，第一个ip才是真实ip
			int index = ip.indexOf(",");
			if (index != -1) {
				return ip.substring(0, index);
			} else {
				return ip;
			}
		}
		ip = request.getHeader("X-Real-IP");
		if (!StringUtils.isEmpty(ip) && !"unKnown".equalsIgnoreCase(ip)) {
			return ip;
		}
		return request.getRemoteAddr();
	}
	
	/**
	 * 功能: 获取当前时间的long类型值
	 * 
	 * @return 日期的long值(秒)
	 * @throws Exception 
	 */
	public static Long getNowTime() throws Exception {
		return CommonUtil.getLongTime(System.currentTimeMillis());
	}
	

	/**
	 * 随机生成数字
	 * @param num 数字位数，如5代表生成5位随机数
	 * @return
	 */
	public static int createRandomNum(int num) throws Exception {
		String v = "1";
		for(int i=0; i<num-1; i++) {
			v += "0";
		}
		int res = (int)((Math.random() * 9 + 1) * Integer.parseInt(v));
		return res;
	}
	
	/**
	 * 验证手机号格式
	 * @param phone 需要验证的手机号
	 * @return
	 * @throws Exception
	 */
	public static boolean checkPhone(String phone) throws Exception {
		String regex = "\\d{11}";
		if(phone.length() != 11){
			return false;
        }else{
            Pattern p = Pattern.compile(regex);
            Matcher m = p.matcher(phone);
            boolean isMatch = m.matches();
            return isMatch;
        }
	}
	
	/**
	 * 断言方法
	 * @param b 断言的判断
	 * @param message 断言说明
	 */
	public static void assertSet(Boolean b, String message) throws Exception{
		boolean assertsEnabled = false;
//		assert assertsEnabled = true;
		if( assertsEnabled ) {
//			LogUtil.writeLog(message);
			assert b:message;
		}
		
	}
	
	/**
	 * 利用反射获取内部类属性
	 * @param c
	 * @param fieldName
	 */
	public static Object reflectInnerClassGetField(Class<?> c, String fieldName, String innerClassName) throws Exception {
		/*利用反射机制获取对象属性*/
		//获取内部类
		Class<?>[] innerClazz = c.getDeclaredClasses();
		//循环内部类
        for(Class<?> claszInner : innerClazz){
        	//比对类名
        	if(innerClassName.equals(claszInner.getSimpleName())) {
        		//类的属性数组
        		Field[] fields = claszInner.getDeclaredFields();
        		//比对属性名
                for(Field field : fields){
                    if(fieldName.equals(field.getName())) {
                    	//属性值
                        Object object = field.get(claszInner);
                    	if(object!=null) {
                    		return object.toString();
                    	}else {
                    		return "";
                    	}
                    	
                    }
                }
        	}
        }
        return "";
	}
	
	/**
     * 首字母大写
     * @param letter
     * @return
     */
    public static String upperFirstLatter(String letter){
        char[] chars = letter.toCharArray();
        if(chars[0]>='a' && chars[0]<='z'){
            chars[0] = (char) (chars[0]-32);
        }
        return new String(chars);
    }
    
    /**
     * map转bean
     * @param map
     * @param beanType
     * @return
     * @throws Exception
     */
    public static <T> T map2bean(Map<Object,Object> map,Class<T> beanType) throws Exception {
		T t=beanType.newInstance();
		PropertyDescriptor [] pds = Introspector.getBeanInfo(beanType,Object.class)
				.getPropertyDescriptors();
		for (PropertyDescriptor pd : pds) {
			for (Entry<Object,Object> entry : map.entrySet()) {
				if(entry.getKey().equals(pd.getName())) {
					pd.getWriteMethod().invoke(t, entry.getValue());
				}
			}
		}
		return t;
	}
    
    /**
     * map转bean
     * @param map
     * @param beanType
     * @return
     * @throws Exception
     */
    public static <T> T map2bean2(Map<String,Object> map,Class<T> beanType) throws Exception {
		T t=beanType.newInstance();
		PropertyDescriptor [] pds = Introspector.getBeanInfo(beanType,Object.class)
				.getPropertyDescriptors();
		for (PropertyDescriptor pd : pds) {
			for (Entry<String,Object> entry : map.entrySet()) {
				//判断有没有下划线，有下划线的情况转成驼峰标识
				String key = entry.getKey();
				if(key.contains("_")) {
					//CaseFormat是引用的 guava库,里面有转换驼峰的,免得重复造轮子
					key = CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.LOWER_CAMEL,key);
				}
				if(key.equals(pd.getName())) {
					pd.getWriteMethod().invoke(t, entry.getValue());
				}
			}
		}
		return t;
	}
	
    /**
     * bean转map
     * @param bean
     * @return
     * @throws Exception
     */
	public static LinkedHashMap<String,Object> bean2map(Object bean) throws Exception {
		
		LinkedHashMap<String,Object> map = new LinkedHashMap<>();
		BeanInfo info=Introspector.getBeanInfo(bean.getClass(),Object.class);
		PropertyDescriptor []pds=info.getPropertyDescriptors();
		for(PropertyDescriptor pd:pds) {
			String key=pd.getName();
			Object value= pd.getReadMethod().invoke(bean);
			if(value!=null) {
				map.put(key, value);
			}

		}		
		return map;		
	}
	
	/**
     * bean转map
     * @param bean
     * @return
     * @throws Exception
     */
	public static Map<String,Object> bean2map2(Object bean) throws Exception {
		
		Map<String,Object> map=new HashMap<>();
		BeanInfo info=Introspector.getBeanInfo(bean.getClass(),Object.class);
		PropertyDescriptor []pds=info.getPropertyDescriptors();
		for(PropertyDescriptor pd:pds) {
			String key=pd.getName();
			Object value= pd.getReadMethod().invoke(bean);
			map.put(key, value);
		}		
		return map;		
	}
	
	/**
	 * 功能:组合出派单流水号
	 * 
	 * @param maxSn
	 *            从数据库查出的最大编号如：NO20170129001
	 * @param pfixName 工单前缀，如SN
	 * @param num 补0的位数
	 * @return
	 */
	public static synchronized String getSN(String pfixName,String maxSn, Integer num) throws Exception {
		String orderNo = null;
		String maxOrderno = maxSn; // 从数据库查询出的最大编号
		if(CommonUtil.isEmpty(pfixName)) {
			pfixName = "NO";
		}
		if(CommonUtil.isEmpty(num) || num == 0) {
			num = 4;
		}
//		System.out.println("maxOrderno=" + maxOrderno);
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd"); // 时间字符串产生方式
		String uid_pfix = pfixName + format.format(new Date()); // 组合流水号前一部分，NO+时间字符串，如：NO20160126
//		System.out.println("time=" + format.format(new Date()));
		if (maxOrderno != null && maxOrderno.contains(uid_pfix)) {
			String uid_end = maxOrderno.substring(10, 14); // 截取字符串最后四位，结果:0001
//			System.out.println("uid_end=" + uid_end);
			int endNum = Integer.parseInt(uid_end); // 把String类型的0001转化为int类型的1
//			System.out.println("endNum=" + endNum);
			int tmpNum = endNum + 1; // 结果2
//			System.out.println("tmpNum=" + tmpNum);
			orderNo = uid_pfix + String.format("%"+num+"d", tmpNum).replace(" ", "0");// 把不足4位的前面补0，再拼成NO201601260002字符串
		} else {
			orderNo = uid_pfix + String.format("%"+num+"d", 1).replace(" ", "0");
		}
		return orderNo;
	}
	
	/**
	 * 判断字符串是否是日期格式
	 * 假设传入的日期格式是yyyy-MM-dd HH:mm:ss, 也可以传入yyyy-MM-dd，如2018-1-1或者2018-01-01格式
	 * @param strDate 日期字符串
	 * @return
	 */
    @SuppressWarnings("unused")
	public static boolean isValidDate(String strDate, String formatContent) throws Exception {
        SimpleDateFormat format = new SimpleDateFormat(formatContent);
        try {
            // 设置lenient为false. 否则SimpleDateFormat会比较宽松地验证日期，比如2018-02-29会被接受，并转换成2018-03-01 
            
            format.setLenient(false);
            Date date = format.parse(strDate);
            
            
        } catch (Exception e) {
            // e.printStackTrace();
            // 如果throw java.text.ParseException或者NullPointerException，就说明格式不对
            return false;
        }
 
        return true;
    }
    
    /**
     * 为指定日期增加指定天数
     * @param date
     * @param day
     * @return
     */
    public static Date dateIncreaseDay(Date date,Integer day) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_MONTH, +day);//+1今天的时间加一天
        date = calendar.getTime();
        return date;
    }

}
