package com.ggjy.util;

import com.alibaba.fastjson.JSONObject;
import com.common.util.privated.util.FastJsonConvertUtil;
import com.common.util.privated.util.HttpRequestUtil;
import com.common.util.privated.vo.HttpResultVo;
import com.ggjy.enums.ErrorMsg;
import com.ggjy.exception.GenerallyException;
import com.ggjy.exception.ParamException;

import java.util.HashMap;
import java.util.Map;

/**
 * 智能柜api接口
 *
 * @author root
 */
@SuppressWarnings({"DuplicatedCode", "RedundantThrows"})
public class IntelligentCabinet {
    /**
     * 合架操作
     * @param url 请求地址
     * @param storeCode 库房代码
     * @param quNo 区号
     */
    public static void reset(String url, String storeCode, int quNo) throws Exception {
        //封装参数
        Map<String, Object> params = new HashMap<>();
        params.put("StoreCode",storeCode);
        params.put("QuNo",quNo);
        //请求地址
        url = url + "/IntelligentCabinetAPIServer/Reset";
        String paramJson = FastJsonConvertUtil.convertObjectToJSONSString(params);
        //调用档案柜
        String body = IntelligentCabinet.excute(url, paramJson);
        JSONObject jsonObj = FastJsonConvertUtil.convertTextToJSONObject(body);
        if(!jsonObj.getString("success").equals("0")) {
            throw new ParamException("操作失败paramJson："+paramJson,null);
        }
    }

    /**
     * 开架
     * @param url 请求地址
     * @param storeCode 库房代码
     * @param quNo 区号
     * @param direction 开架方向
     * @param num 需要打开的列
     */
    public static void open(String url, String storeCode, int quNo, int direction, int num) throws Exception {
        //封装参数
        Map<String, Object> params = new HashMap<>();
        params.put("StoreCode",storeCode);
        params.put("QuNo",quNo);
        params.put("ColNo",num);
        //请求地址
        if(direction==1) {
            url = url + "/IntelligentCabinetAPIServer/MoveLeft";
        }else if(direction==2) {
            url = url + "/IntelligentCabinetAPIServer/MoveRight";
        }

        String paramJson = FastJsonConvertUtil.convertObjectToJSONSString(params);
        //调用档案柜
        String body = IntelligentCabinet.excute(url, paramJson);
        JSONObject jsonObj = FastJsonConvertUtil.convertTextToJSONObject(body);
        if(!jsonObj.getString("success").equals("0")) {
            throw new ParamException("操作失败paramJson："+paramJson+"=====body:"+body,null);
        }
    }

    /**
     * 查看状态
     * @param url 请求地址
     * @param storeCode 库房代码
     * @param quNo 区号
     */
    public static JSONObject getStatus(String url, String storeCode, int quNo) throws Exception {
        //封装参数
        Map<String, Object> params = new HashMap<>();
        params.put("StoreCode",storeCode);
        params.put("QuNo",quNo);
        //请求地址
        url = url + "/IntelligentCabinetAPIServer/ReportStatus";
        String paramJson = FastJsonConvertUtil.convertObjectToJSONSString(params);
        //查询状态
        String body = IntelligentCabinet.excute(url, paramJson);
        JSONObject jsonObj = FastJsonConvertUtil.convertTextToJSONObject(body);
        if(!jsonObj.getString("success").equals("0")) {
            throw new ParamException("操作失败paramJson："+paramJson+"=====body:"+body,null);
        }
        return jsonObj;
    }

    /**
     * 唤醒操作
     * @param url 请求地址
     * @param storeCode 库房代码
     * @param quNo 区号
     */
    public static void awake(String url, String storeCode, int quNo) throws Exception {
        //封装参数
        Map<String, Object> params = new HashMap<>();
        params.put("StoreCode",storeCode);
        params.put("QuNo",quNo);
        //请求地址
        url = url + "/IntelligentCabinetAPIServer/Awake";
        String paramJson = FastJsonConvertUtil.convertObjectToJSONSString(params);
        //唤醒操作
        IntelligentCabinet.excute(url, paramJson);
        Thread.sleep(3000);
//        JSONObject jsonObj = FastJsonConvertUtil.convertTextToJSONObject(body);
//        if(!jsonObj.getString("success").equals("0")) {
//            throw new ParamException("操作失败paramJson："+paramJson+"=====body:"+body,null);
//        }
    }

    /**
     * 操作档案柜
     * @param url 请求接口
     * @param paramJson 请求参数
     * @throws Exception 抛出异常
     */
    protected static String excute(String url, String paramJson) throws Exception {
        HttpResultVo result;
        try {
            result = HttpRequestUtil.sendSpringHttpPost(url ,paramJson, null);
        }catch (Exception e) {
            throw new GenerallyException(ErrorMsg.PARAM_ERROR.getId(),"与密集柜之间的网络连接超时，请检查网络",
                    null,null);
        }

        return result.getBody();
    }

}
