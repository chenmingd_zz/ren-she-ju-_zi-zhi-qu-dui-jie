package com.ggjy.util;

import com.common.util.privated.util.CommonUtil;
import com.common.util.privated.vo.HttpResultVo;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * TODO
 *
 * @author root
 * @date 2020/9/29
 */
public class HttpRequestUtil {
    /**
     * 发送同步post请求
     * @param url 请求的url
     * @param map 传递的参数
     * @throws Exception
     */
    public static HttpResultVo sendSynHttpPost(String url, Map<String,Object> map, HttpServletRequest request) throws Exception {
        //返回参数
        HttpResultVo result = new HttpResultVo();

        RestTemplate restTemplate = new RestTemplate();
        LinkedMultiValueMap<String, Object> body = new LinkedMultiValueMap<String, Object>();
        Set<String> keys = map.keySet();
        Iterator<String> it = keys.iterator();
        //组装参数
        while(it.hasNext()) {
            String key = it.next();
            body.add(key,map.get(key));
        }
        //设置head
        HttpHeaders headers = new HttpHeaders();
        //设置cook
        Cookie[] cookArr = request.getCookies();
        if(cookArr!=null) {
            List<String> cookies = new ArrayList<String>();
            for (Cookie cook : cookArr) {
                cookies.add(cook.getName()+"="+cook.getValue());
            }
            headers.put(HttpHeaders.COOKIE,cookies);
        }
        headers.add("Accept", MediaType.APPLICATION_JSON.toString());
        //headers.add("Authorization",map.get("Authorization").toString());
        if(!CommonUtil.isEmpty(map) && !CommonUtil.isEmpty(map.get("Authorization"))) {
            headers.add("Authorization",map.get("Authorization").toString());
        }

        HttpEntity<LinkedMultiValueMap<String, Object>> httpEntity = new HttpEntity<LinkedMultiValueMap<String, Object>>(body,headers);
        //发送请求
        ResponseEntity<String> re =  restTemplate.postForEntity(url, httpEntity, String.class);
        //返回结果
        result.setBody(re.getBody());
        result.setStatus(re.getStatusCodeValue()+"");
        return result;
    }

    /**
     * 发送spring httppost请求
     * @param url 请求的url
     * @param map 传递的参数
     * @throws Exception
     */
    public static HttpResultVo sendSpringHttpPost(String url,Map<String,Object> map, List<String> cookies) throws Exception {
        //返回参数
        HttpResultVo result = new HttpResultVo();

        RestTemplate restTemplate = new RestTemplate();
        LinkedMultiValueMap<String, Object> body = new LinkedMultiValueMap<String, Object>();
        Set<String> keys = map.keySet();
        Iterator<String> it = keys.iterator();
        //组装参数
        while(it.hasNext()) {
            String key = it.next();
            body.add(key,map.get(key));
        }
        //设置head
        HttpHeaders headers = new HttpHeaders();
        if(cookies!=null && cookies.size()>0) {
            headers.put(HttpHeaders.COOKIE,cookies);
        }
        MediaType type = MediaType.parseMediaType("application/x-www-form-urlencoded; charset=UTF-8");
        headers.setContentType(type);
        headers.add("Accept", MediaType.APPLICATION_JSON.toString());
        if(!CommonUtil.isEmpty(map) && !CommonUtil.isEmpty(map.get("Authorization"))) {
            headers.add("Authorization",map.get("Authorization").toString());
        }

        HttpEntity<LinkedMultiValueMap<String, Object>> httpEntity = new HttpEntity<LinkedMultiValueMap<String, Object>>(body,headers);
        //发送请求
        ResponseEntity<String> re =  restTemplate.postForEntity(url, httpEntity, String.class);
        //返回结果
        result.setBody(re.getBody());
        result.setStatus(re.getStatusCodeValue()+"");
        return result;
    }

    /**
     * 发送spring httppost请求 json参数
     * @param url 请求的url
     * @param jsonStr 传递的参数
     * @throws Exception
     */
    public static HttpResultVo sendSpringHttpPost(String url,String jsonStr, List<String> cookies) throws Exception {
        //返回参数
        HttpResultVo result = new HttpResultVo();

        RestTemplate restTemplate = new RestTemplate();

        //设置head
        HttpHeaders headers = new HttpHeaders();
        if(cookies!=null && cookies.size()>0) {
            headers.put(HttpHeaders.COOKIE,cookies);
        }
        headers.setContentType(MediaType.APPLICATION_JSON);
//        headers.add("Accept", MediaType.APPLICATION_JSON.toString());

        HttpEntity<String> httpEntity = new HttpEntity<>(jsonStr,headers);
        //发送请求
        ResponseEntity<String> re =  restTemplate.postForEntity(url, httpEntity, String.class);
        //返回结果
        result.setBody(re.getBody());
        result.setStatus(re.getStatusCodeValue()+"");
        return result;
    }

    /**
     * 发送同步get请求
     * @param url 请求的url
     * @param map 传递的参数
     * @throws Exception
     */
    public static HttpResultVo sendSynHttpGet(String url,Map<String,Object> map, HttpServletRequest request) throws Exception {
        //返回参数
        HttpResultVo result = new HttpResultVo();

        RestTemplate restTemplate = new RestTemplate();
        //发送请求
        ResponseEntity<String> re =  restTemplate.getForEntity(url, String.class);

        //返回结果
        result.setBody(re.getBody());
        result.setStatus(re.getStatusCodeValue()+"");
        return result;
    }

    /**
     * 发送spring httpget请求
     * @param url 请求的url
     * @param map 传递的参数
     * @throws Exception
     */
    public static HttpResultVo sendSpringHttpGet(String url,Map<String,Object> map) throws Exception {
        //返回参数
        HttpResultVo result = new HttpResultVo();

        RestTemplate restTemplate = new RestTemplate();
        //发送请求
        ResponseEntity<String> re =  restTemplate.getForEntity(url, String.class);

        //返回结果
        result.setBody(re.getBody());
        result.setStatus(re.getStatusCodeValue()+"");
        return result;
    }
}
