package com.ggjy.dao.master;

import com.ggjy.entity.PositionInfo;
import com.ggjy.entity.UnitBasicInfo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface UnitBasicInfoMapper {

    /**
     * 查询公司信息
     * @return 公司信息
     * @throws Exception 抛出异常
     */
    List<UnitBasicInfo> getListCompany() throws Exception;

    /**
     * 查询岗位信息
     * @param paramMap 查询参数
     * @return 岗位信息
     * @throws Exception 抛出异常
     */
    List<PositionInfo> getListPosition(Map<String, Object> paramMap) throws Exception;

    /**
     * 查询简历信息
     * @return 简历信息
     * @throws Exception 抛出异常
     */
    List<Map<String, Object>> getResumeList() throws Exception;

    /**
     * 天地图查询岗位信息
     * @param paramMap 查询参数
     * @return 岗位信息
     * @throws Exception 抛出异常
     */
    List<PositionInfo> getListPositionTianditu(Map<String, Object> paramMap) throws Exception;

    /**
     * 查询招聘会与职位相关信息
     * @param paramMap 查询参数
     * @return 招聘会职位信息
     * @throws Exception 抛出异常
     */
    List<Map<String, Object>> getFairForPositionList(Map<String, Object> paramMap) throws Exception;

    /**
     * 查询招聘会与简历相关信息
     * @param params 查询参数
     * @return 招聘会简历信息
     * @throws Exception 抛出异常
     */
    List<Map<String, Object>> getFairForResumeList(Map<String, Object> params) throws Exception;
}