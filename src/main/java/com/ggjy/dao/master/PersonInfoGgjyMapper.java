package com.ggjy.dao.master;

import com.ggjy.entity.PersonInfo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * TODO
 *
 * @author root
 * @date 2021/1/18
 */
@Mapper
public interface PersonInfoGgjyMapper {
    /**
     * 查询企业列表信息
     * @return 企业信息
     * @throws Exception 抛出异常
     */
    List<PersonInfo> getListCompany() throws Exception;
}
